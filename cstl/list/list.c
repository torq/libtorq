//
// Created by torq on 28/09/18.
//

#include "list.h"
#include <stdlib.h>

torq_list_node* torq_list_node_create(void *const value, torq_list_node *const next) {
    torq_list_node *new_node = (torq_list_node *)malloc(sizeof(torq_list_node));
    *new_node = (torq_list_node){ value, next };
    return new_node;
}

torq_list* torq_list_create() {
    torq_list *new_list = (torq_list *)malloc(sizeof(torq_list));
    return new_list;
}

torq_list_node* torq_list_get(int index, const torq_list *const list) {
    if (index < 0 || index >= list->count)
        return NULL;

    torq_list_node *temp = list->nodes;
    for(int i=0; i<index; ++i)
        temp = temp->next;
    return temp;
}

void torq_list_push_front(void *const value, torq_list *const list) {
    torq_list_node *new_node = torq_list_node_create(value, list->first);
    list->nodes = new_node;
    list->first = list->nodes;
    if(!list->last)
        list->last = list->first;
    ++(list->count);
}

void torq_list_push_back(void *const value, torq_list *const list) {
    torq_list_node *new_node = torq_list_node_create(value, NULL);
    list->last->next = new_node;
    list->last = new_node;
    if(!list->first)
        list->first = list->last;
    ++(list->count);
}

void torq_list_push(void *const value, int index, torq_list *const list) {
    if(index <= 0){
        torq_list_push_front(value, list);
    } else if(index >= list->count){
        torq_list_push_back(value, list);
    } else {
        torq_list_node *new_node = torq_list_node_create(value, NULL);
        torq_list_node *antecessor = torq_list_get(index - 1, list);
        new_node->next = antecessor->next;
        antecessor->next = new_node;
        ++(list->count);
    }
}

void torq_list_update(void *const value, int index, torq_list *const list) {
    torq_list_node *node = torq_list_get(index, list);
    if(node)
        node->value = value;
}

void* torq_list_pop_unique(torq_list *const list) {
    torq_list_node *node = list->first;
    list->nodes = NULL;
    list->first = NULL;
    list->last = NULL;
    list->count = 0;

    void *value = node->value;
    free(node);
    return value;
}

void* torq_list_pop_front(torq_list *const list) {
    if(!list->count)
        return NULL;
    if(list->count == 1)
        return torq_list_pop_unique(list);

    torq_list_node *node = list->first;
    list->first = node->next;

    void *value = node->value;
    free(node);
    return value;
}

void* torq_list_pop_back(torq_list *const list) {
    if(!list->count)
        return NULL;
    if(list->count == 1)
        return torq_list_pop_unique(list);

    torq_list_node *node = list->last;
    list->last = torq_list_get(list->count - 2, list);
    list->last->next = NULL;

    void *value = node->value;
    free(node);
    return value;
}

void* torq_list_pop(int index, torq_list *const list) {
    if(index <= 0)
        return torq_list_pop_back(list);
    if(index >= list->count)
        return torq_list_pop_front(list);
    if(!list->count)
        return NULL;
    if(list->count == 1)
        return torq_list_pop_unique(list);

    torq_list_node *antecessor = torq_list_get(index - 1, list);
    torq_list_node *node = antecessor->next;
    antecessor->next = node->next;
    --(list->count);

    void *value = node->value;
    free(node);

    return value;
}

void torq_list_clear(torq_list *const list) {
    while(list->nodes) {
        torq_list_node *temp = list->nodes;
        list->nodes = list->nodes->next;
        free(temp->value);
        free(temp);
    }
    list->nodes = NULL;
    list->first = NULL;
    list->last = NULL;
    list->count = 0;
}

torq_list* torq_list_map(const torq_list *const list, void *(*map_func)(void *)){
    torq_list* new_list = torq_list_create();
    for(torq_list_node *temp = list->nodes; temp; temp = temp->next) {
        void *new_value = (*map_func)(temp->value);
        torq_list_push_back(new_value, new_list);
    }
    return new_list;
}

void torq_list_destructive_map(torq_list *list, void *(*map_func)(void *)){
    for(torq_list_node *temp = list->nodes; temp; temp = temp->next)
        temp->value = (*map_func)(temp->value);
}

torq_list* torq_list_filter(const torq_list *const list, int (*filter_func)(void *)){
    torq_list* new_list = torq_list_create();
    for(torq_list_node *temp = list->nodes; temp; temp = temp->next)
        if((*filter_func)(temp->value))
            torq_list_push_back(temp->value, new_list);

    return new_list;
}

void* torq_list_reduce(const torq_list *const list, void *(*reduce_func)(void *, void *), void *init_val){
    void* acc = init_val;
    for(torq_list_node *temp = list->nodes; temp; temp = temp->next)
        acc = (*reduce_func)(temp->value, acc);
    return acc;
}

void torq_list_foreach(const torq_list *const list, void (*foreach_func)(void *)){
    for(torq_list_node *temp = list->nodes; temp; temp = temp->next)
        (*foreach_func)(temp->value);
}