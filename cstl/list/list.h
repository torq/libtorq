//
// Created by torq on 28/09/18.
//

#ifndef LIBTORQ_LIST_H
#define LIBTORQ_LIST_H

typedef struct ListNode {
    void *value;
    struct ListNode *next;
} torq_list_node;

torq_list_node* torq_list_node_create(void *const value, torq_list_node *const next);

typedef struct List {
    torq_list_node *nodes;
    torq_list_node *first;
    torq_list_node *last;
    int count;
}torq_list;

torq_list* torq_list_create();

torq_list_node* torq_list_get(int index, const torq_list *const list);

void torq_list_push_front(void *const value, torq_list *const list);

void torq_list_push_back(void *const value, torq_list *const list);

void torq_list_push(void *const value, int index, torq_list *const list);

void torq_list_update(void *const value, int index, torq_list *const list);

void* torq_list_pop_front(torq_list *const list);

void* torq_list_pop_back(torq_list *const list);

void* torq_list_pop(int index, torq_list *const list);

void torq_list_clear(torq_list *const list);

torq_list* torq_list_map(const torq_list *const list, void *(*map_func)(void *));

void torq_list_destructive_map(torq_list *list, void *(*map_func)(void *));

torq_list* torq_list_filter(const torq_list *const list, int (*filter_func)(void *));

void* torq_list_reduce(const torq_list *const list, void *(*reduce_func)(void *, void *), void *init_val);

void torq_list_foreach(const torq_list *const list, void (*foreach_func)(void *));

#endif //LIBTORQ_LIST_H
