//
// Created by torq on 29/09/18.
//

#include "string.h"

#include <string.h>

#include "../alloc/alloc.h"

char* torq_replace(const char* const input, const char replaceable, const char replacer) {
    size_t len = strlen(input);
    char* new = (char *)torq_malloc_soft(len+1, sizeof(char));
    if(!new) {
        return NULL;
    }

    for(int i=0; i<=len; ++i) {
        new[i] = (input[i] == replaceable)
                 ? replacer
                 : input[i];
    }

    return new;
}

unsigned int torq_count_char(const char *const input, char token) {
    unsigned c = 0;
    for(const char* temp = input; *temp; ++temp)
        if(*temp == token)
            ++c;
    return c;
}

int* torq_find_matches(const char *const input, char token, unsigned *count) {
    *count = torq_count_char(input, token);

    int *matches = (int *)torq_malloc_soft(*count, sizeof(int));
    if(!matches) {
        *count = 0;
        return NULL;
    }

    for(int i=0, j=0; input[i]; ++i)
        if(input[i] == token)
            matches[j++] = i;
    return matches;
}

char** torq_split(const char *const input, char delimiter, int* const n_lines) {
    int n_tokens;
    int *matches = torq_find_matches(input, delimiter, &n_tokens);

    *n_lines = n_tokens + 1;
    char **lines = (char **)torq_malloc_soft(*n_lines, sizeof(char *));
    if(!lines) {
        *n_lines = 0;
        return NULL;
    }

    int k = 0;
    int len = strlen(input);
    for(int i=0, j=0; i<len; ++j) {
        int index = (j < n_tokens) ? matches[j] : len;
        int diff = (index - i);
        if(diff <= 0) {
            i = index + 1;
            continue;
        }

        char *buffer = (char *)torq_malloc_soft((diff+1), sizeof(char));
        if(!buffer) {
            *n_lines = 0;
            return NULL;
        }

        strncpy(buffer, input+i, diff);
        buffer[diff] = NULL;
        lines[k++] = buffer;
        i = index + 1;
    }

    *n_lines = k;
    lines = (char **)torq_realloc_soft(lines, k, sizeof(char *));

    return lines;
}