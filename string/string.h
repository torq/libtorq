//
// Created by torq on 29/09/18.
//

#ifndef LIBTORQ_STRING_H
#define LIBTORQ_STRING_H

unsigned int torq_count_char(const char *const input, char token);
int* torq_find_matches(const char *const input, char token, unsigned *count);
char* torq_replace(const char* const input, const char replaceable, const char replacer);
char** torq_split(const char *const input, char delimiter, int* const n_lines);

#endif //LIBTORQ_STRING_H
