//
// Created by torq on 29/09/18.
//

#include "file.h"

#include <stdlib.h>

#include "defs.h"
#include "../string/string.h"

void torq_open_file_hard(const char *path, const char *mode, FILE *file) {
    file = fopen(path, mode);
    if (!file) {
        fprintf(stderr, "Unable to open %s\n", path);
        exit(ERR_FILE_INIT);
    }
}

char* torq_file_to_string(FILE *file) {
    // get file size
    fseek(file, 0, SEEK_END);
    long fsize = ftell(file);
    fseek(file, 0, SEEK_SET);

    // store the entire file inside a string
    char *string = malloc(fsize+1);
    fread(string, fsize, 1, file);
    string[fsize] = 0;

    return string;
}

int torq_file_to_strings(FILE *file, char **lines) {
    char *string = torq_file_to_string(file);

    int n_lines;
    lines = torq_split(string, '\n', &n_lines);

    return n_lines;
}
