//
// Created by torq on 29/09/18.
//

#ifndef LIBTORQ_FILE_H
#define LIBTORQ_FILE_H

#include <stdio.h>

/**
 * Open a file with a path with mode. If it fails, exit the program immediatelly.
 * Useful for files that are essetial to the program.
 * @param path Path where the file is(or will be) located.
 * @param mode Mode of the file.
 * @param file File handler.
 */
void torq_open_file_hard(const char *path, const char *mode, FILE *file);

/**
 * Transform a file into a string
 * @param file handler of the file to be read
 * @return string containing the file file content
 */
char* torq_file_to_string(FILE *file);

/**
 * Reads a file and transforms it into an array of lines
 * @param file handler of the file to be read
 * @param lines the file as lines (output)
 * @return number of lines
 */
int torq_file_to_strings(FILE *file, char **lines);

#endif //LIBTORQ_FILE_H
