//
// Created by torq on 29/09/18.
//

#ifndef LIBTORQ_ALLOC_H
#define LIBTORQ_ALLOC_H

void* torq_malloc_hard(unsigned long alloc_size, unsigned long type_size);
void* torq_malloc_soft(unsigned long alloc_size, unsigned long type_size);

void* torq_calloc_hard(unsigned long alloc_size, unsigned long type_size);
void* torq_calloc_soft(unsigned long alloc_size, unsigned long type_size);

void* torq_realloc_hard(void *container, unsigned long alloc_size, unsigned long type_size);
void* torq_realloc_soft(void *container, unsigned long alloc_size, unsigned long type_size);

#endif //LIBTORQ_ALLOC_H
