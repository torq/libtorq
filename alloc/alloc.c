//
// Created by torq on 29/09/18.
//

#include "alloc.h"

#include <stdio.h>
#include <stdlib.h>

#include "defs.h"

void check_alloc_hard(void *container) {
    if(!container) {
        fprintf(stderr, "Unable to allocate\n");
        exit(ERR_ALLOC);
    }
}

void* torq_malloc_hard(size_t alloc_size, size_t type_size) {
    void *container = malloc(alloc_size*type_size);
    check_alloc_hard(container);
    return container;
}

void* torq_malloc_soft(size_t alloc_size, size_t type_size){
    void *container = malloc(alloc_size*type_size);
    return container;
}

void* torq_calloc_hard(size_t alloc_size, size_t type_size) {
    void *container = calloc(alloc_size, type_size);
    check_alloc_hard(container);
    return container;
}

void* torq_calloc_soft(size_t alloc_size, size_t type_size) {
    void *container = calloc(alloc_size, type_size);
    return container;
}

void* torq_realloc_hard(void *container, size_t alloc_size, size_t type_size) {
    void* new_container = realloc(container, alloc_size*type_size);
    check_alloc_hard(new_container);
    return new_container;
}

void* torq_realloc_soft(void *container, size_t alloc_size, size_t type_size) {
    void* new_container = realloc(container, alloc_size*type_size);
    return new_container;
}
